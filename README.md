ALang
==================
    Yet Another Programming Language.
    Features:
        - Object oriented.
        - Syntax similar to Python & SLang.

    www.aayushpoudel.info/alang

Build dependencies.
-------------------
    - flex: Lexical analysis.
      $ sudo aptitude install flex

    - bison: Parser genarator.
      $ sudo aptitude install bison

    - boost: Boost C++ library.
      $ sudo aptitude install libboost-dev

    - cmake: Build tool
      $ sudo aptitude install cmake
      
    - Build: build-essential 
      $ sudo aptitude install build-essential -y 

To generate documentation.
----------------------------
    - doxygen: To generae doc.
      $ sudo aptitude install doxygen
      
    - graphviz: To generate class diagrame.   
      $ sudo aptitude install graphviz
    
    - To generate doc, inside /path/to/ALang/Resources/Doc/
      $ doxygen doxygen.cfg

How to build.
-------------------
    - Inside /path/to/ALang
      $ cmake .
      $ make

How to run.
-------------------
    - Inside /path/to/ALang
      $ Src/ALang Resources/Example/Script.1.al

If Lexer.l and Parser.y got modified, generate new Parser.cpp & Lexer.cpp like.
----------------------------------------
    Inside Src/Parser
    $ bison --defines=../../Include/Parser/Parser.h --output=Parser.cpp Parser.y
    
    Inside Src/Lexer
    flex --outfile=Lexer.cpp Lexer.l
