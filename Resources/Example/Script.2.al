#!/usr/bin/ALang
#---------------------------------------------------------------
# ALang is a case-sensitive typeless language.
#---------------------------------------------------------------

# This is how you define a function
Func fixed_arg_func( a, b, c )
    Print( "Hello from FIXED argument func", a, b )

    # You can define nested function too.
    Func nested_func( a )
        Print( "Hello from NESTED function", a )

    # Function call
    nested_func( c )

# This function takes variable number of arguments
Func var_arg_func( arg ... )
    Print( "Hello from VARIABLE argument func" )
    Print( "Printing Arg ", arg )

# Define variables
age = 12.5
name = "Ram Poudel"
address = '1400 Pennsylvania Ave, DC'
zip = 31117

# Function call
fixed_arg_func( address, age, name )
var_arg_func( age, name, address, zip, 1, 'Hello', 3, 4, "World", 3 )
