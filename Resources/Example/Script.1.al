#!/usr/bin/ALang
#---------------------------------------------------------------
# ALang is a case-sensitive typeless language.
#---------------------------------------------------------------

# This is how you define a function
Func add( a, b )
    Print( a + b )

add( 2.12, 3.43 )
add( "Hello ", 'World' )
add( [ 1, 'Aayush', 3 ], [ 5, "Poudel", 7, 8 ] )
