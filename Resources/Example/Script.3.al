#!/usr/bin/ALang
#---------------------------------------------------------------
# ALang is a case-sensitive typeless language.
#---------------------------------------------------------------

arr = [ 1, 'Hello', 2, "World" ]

Print( "Iterating using For() statement." )

#Iterate Array
For( val : arr )
    Print( val )

x = 0

Print( "Iterating using While() statement." )

While x < Size( arr )
    Print( arr[ x ] )
    x = x + 1

# Define a function
Func calc_factorial( num )
    If num == 0
        Return 1

    # You don't have to write Return if it is a last statement
    num * calc_factorial( num - 1 )

Print( "Fatorial:", calc_factorial( 5 ))
