%{
#include <string>
#include <cstdlib>
#include <iostream>

#include "Parser/Ast.hpp"

ALang::Ast::NBlock *Program;

extern int yylex();
void yyerror( const char *S )
{
    std::cout << "Error: " <<  S << "\n";
    std::exit(1);
}
%}

%error-verbose

/* Represents the many different ways we can access our data */
%union {
	ALang::Ast::Node 		*Node;
	ALang::Ast::NBlock 		*Block;
	ALang::Ast::NExpression *Expr;
	ALang::Ast::NStatement 	*Stmt;
	ALang::Ast::NIdentifier *Ident;
	
	ALang::Ast::ParameterList 	*ParaList;
	ALang::Ast::ExpressionList 	*ExprList;
	ALang::Ast::IfStatementList *IfList;
	
	std::string 	*String;
	unsigned int 	Token;
}

/* Define our terminal symbols (tokens). This should
 *  match our Lexer.l lex file. We also define the node type
 *  they represent.
 */
%token <String> TIDENTIFIER TINTEGER TDOUBLE TSTRING
%token <Token>  TCEQ TCNE TCLT TCLE TCGT TCGE TEQUAL
%token <Token>  TLPAREN TRPAREN TLBRACE TRBRACE TLBRACKET TRBRACKET
%token <Token>  TNOP TCOMMA TDOT TELLIPSIS TINDENT TDEDENT TCOLON
%token <Token>  TPLUS TMINUS TMUL TDIV TPOW
%token <Token>  TEOL TRETURN TIF TELSE TELIF TWHILE TFOR TFUNC

/* Define the type of node our nonterminal symbols represent.
 *  The types refer to the %union declaration above.
 */

%type <Ident> Identifier
%type <Expr> Expression Number String
%type <ExprList> ExpressionList
%type <ParaList> ParameterList
%type <IfList> IfStatementList
%type <Block> Program Statements Block
%type <Stmt> Statement IfStatement
%type <Token> Comparison

/* Operator precedence for mathematical operators */
%nonassoc TCEQ TCNE TCLT TCLE TCGT TCGE

%right  TEQUAL 
%left   TPLUS TMINUS
%left   TMUL TDIV
%right  TPOW

%start Program

%%
Program : Statements { Program = $1; }
		;

Statements : Statement  { $$ = new ALang::Ast::NBlock(); $$->Statements.push_back( $1 ); }
    | Statements Statement  { $1->Statements.push_back( $2 ); }
    ;

Statement : Expression TEOL     { $$ = new ALang::Ast::NExpressionStatement( *$1 ); }
    | TRETURN TEOL              { $$ = new ALang::Ast::NReturnStatement(); }
    | TRETURN Expression TEOL   { $$ = new ALang::Ast::NReturnExpressionStatement( *$2 ); }
    | TWHILE Expression Block   { $$ = new ALang::Ast::NWhileStatement( *$2, *$3 ); }
    | IfStatement               { $$ = $1; }
    | TFOR TLPAREN Identifier TCOLON Expression TRPAREN Block { $$ = new ALang::Ast::NForStatement( *$3, *$5, *$7 );  }
    | TFUNC Identifier TLPAREN Identifier TELLIPSIS TRPAREN Block {
            ALang::Ast::ParameterList 	P { $4 };
            $$ = new ALang::Ast::NUserFunctionDefinition( *$2, P, *$7, ALang::Ast::ArgumentType::VARIABLE_LENGTH );
        }
    | TFUNC Identifier TLPAREN ParameterList TRPAREN Block {
            $$ = new ALang::Ast::NUserFunctionDefinition( *$2, *$4, *$6, ALang::Ast::ArgumentType::FIXED_LENGTH );
            delete $4;
        }
    ;

IfStatement : IfStatementList  { $$ = new ALang::Ast::NIfStatement( *$1 ); delete $1; }
    | IfStatementList TELSE Block { $$ = new ALang::Ast::NIfElseStatement( *$1, *$3 ); delete $1; }
    ;

IfStatementList : TIF Expression Block {
            $$ = new ALang::Ast::IfStatementList();
            $$->push_back( std::make_pair( $2, $3 ));
        }
    | IfStatementList TELIF Expression Block { $$->push_back( std::make_pair( $3, $4 )); }
    ;

Block : TINDENT Statements TDEDENT  { $$ = $2; }
	;

Identifier : TIDENTIFIER { $$ = new ALang::Ast::NIdentifier( *$1 ); }
	;

Number : TINTEGER { $$ = new ALang::Ast::NInteger( *$1 ); delete $1; }
	| TDOUBLE { $$ = new ALang::Ast::NDouble( *$1 ); delete $1; }
	;

String : TSTRING { $$ = new ALang::Ast::NString( *$1 ); }
	;

Expression : Identifier TEQUAL Expression       { $$ = new ALang::Ast::NAssignment( *$1, *$3 ); }
    | Identifier TLPAREN ExpressionList TRPAREN { $$ = new ALang::Ast::NFunctionCall( *$1, *$3 ); delete $3; }
    | Expression TLBRACKET Expression TRBRACKET { $$ = new ALang::Ast::NArrayAccess( *$1, *$3 ); }
    | Expression TMUL   Expression              { $$ = new ALang::Ast::NBinaryOperator( *$1, $2, *$3 ); }
    | Expression TDIV   Expression              { $$ = new ALang::Ast::NBinaryOperator( *$1, $2, *$3 ); }
    | Expression TPLUS  Expression              { $$ = new ALang::Ast::NBinaryOperator( *$1, $2, *$3 ); }
    | Expression TMINUS Expression              { $$ = new ALang::Ast::NBinaryOperator( *$1, $2, *$3 ); }
    | Expression TPOW   Expression              { $$ = new ALang::Ast::NBinaryOperator( *$1, $2, *$3 ); }
    | Expression Comparison Expression          { $$ = new ALang::Ast::NBinaryOperator( *$1, $2, *$3 ); }
    | TLPAREN Expression TRPAREN                { $$ = $2; }
    | TLBRACKET ExpressionList TRBRACKET        { $$ = new ALang::Ast::NArrayDefination( *$2 ); delete $2; }
    | Number
    | String
    | Identifier { $<Ident>$ = $1; }
	;

ExpressionList: /* Blank */                 { $$ = new ALang::Ast::ExpressionList(); }
    | Expression                            { $$ = new ALang::Ast::ExpressionList(); $$->push_back( $1 ); }
    | ExpressionList TCOMMA Expression      { $1->push_back( $3 ); }
    ;

ParameterList: /* Blank */                  { $$ = new ALang::Ast::ParameterList(); }  
    | Identifier                            { $$ = new ALang::Ast::ParameterList(); $$->push_back( $1 );  }
    | ParameterList TCOMMA Identifier	    { $1->push_back( $3 ); }
    ;

Comparison : TCEQ | TCNE | TCLT | TCLE | TCGT | TCGE;

%%
